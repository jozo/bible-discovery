document.addEventListener('deviceready', onDeviceReady, false)

function onDeviceReady () {
  window.open = cordova.InAppBrowser.open
}

$(document).ready(function () {
  $('.toggle-submenu').click(function (event) {
    $(event.currentTarget).siblings().toggle()
  })

  $('.menu-toggle').click(function (event) {
    $('#main-nav').toggle()
    $(event.currentTarget).toggleClass('invert')
  })
})
